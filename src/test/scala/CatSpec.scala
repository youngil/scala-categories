package category

import org.scalatest._
import org.scalatest.funsuite.AnyFunSuite

trait CategoryLaws extends GivenWhenThen { this: AnyFunSuite =>

  def verify_category[O,A](cat1: Category[O,A], name: String) = {

    import cat1.{objects, arrows, domain, codomain, id, comp, arrows2, arrows3}

    test(s"$name: Objects, Arrows, Domain, Codomain, and Id") {
      Given(s"""Objects:\n${objects.mkString("\n")}""")
      Given(s"""Arrows:\n${arrows.mkString("\n")}""")
      arrows.foreach(a => Given(s"domain($a) is ${domain(a)}"))
      arrows.foreach(a => Given(s"codomain($a) is ${codomain(a)}"))
      objects.foreach(x => Given(s"id($x) is ${id(x)}"))
      Then("for all object x: domain(id(x)) == x")
      assert(cat1.domain_id())
      And("for all object x: codomain(id(x)) == x")
      assert(cat1.codomain_id()) }

    test(s"$name: Composition of Arrows") {
      arrows2.foreach { case (g, f) =>
        val fg = comp((g,f))
        Given(s"f = $f")
        Given(s"g = $g")
        Given(s"domain(f): ${domain(f)}")
        Given(s"codomain(g): ${codomain(g)}")
        Given(s"comp(g,f) = $fg")
        Given(s"domain(comp(g,f)): ${domain(comp(g,f))}")
        Given(s"codomain(comp(g,f)): ${codomain(comp(g,f))}") }
      Then("domain(comp(g,f)) == domain(f)")
      assert(cat1.domain_comp())
      And("codomain(comp(g,f)) == codomain(g)")
      assert(cat1.codomain_comp()) }

    test(s"$name: Composition is Associative") {
      arrows3.foreach { case (h,g,f) =>
        Given(s"f: $f")
        Given(s"g: $g")
        Given(s"h: $h")
        Given(s"comp(h,comp(g,f)): ${comp(h,comp((g,f)))}")
        Given(s"comp(comp(h,g),f): ${comp(comp((h,g)),f)}") }
      Then("comp(h,comp(g,f)) == comp(comp(h,g),f)")
      assert(cat1.comp_associative()) }

    test(s"$name: Identity Laws") {
      arrows.foreach { f =>
        val ident1 = id(domain(f))
        val ident2 = id(codomain(f))
        Given(s"f = $f")
        Given(s"ident1 on domain: $ident1")
        Given(s"ident2 on codomain: $ident2") }
      Then("comp(ident2,f) == f  && comp(f, ident1) == f")
      assert(cat1.comp_id()) }
  }

  def verify_initial[O,A](cat1: CategoryInit[O,A], name: String) = {

    import cat1.{objects, arrows, domain, codomain, id, comp, arrows2, arrows3, initial}

    test(s"$name: Initial Object") {
      Given(s"""Objects:\n${objects.mkString("\n")}""")
      Given(s"""Arrows:\n${arrows.mkString("\n")}""")
      Given(s"""InitialObj:\n${initial}""")
      arrows.foreach(a => Given(s"domain($a) is ${domain(a)}"))
      arrows.foreach(a => Given(s"codomain($a) is ${codomain(a)}"))
      objects.foreach(x => Given(s"id($x) is ${id(x)}"))
      Then("for all object x: domain(initial.uniq(x)) == initial.obj && codomain(initial.uniq(x)) == x")
      assert(cat1.initial_object()) }

    test(s"$name: Initial Object Unique Upto Iso") {
      Given(s"""InitialObj:\n${initial}""")
      Then("id(o1) == comp(univ2(o1), univ1(o2)) && id(o2) == comp(univ1(o2), univ2(o1))")
      assert(cat1.initial_unique(cat1.initial, cat1.initial)) } 
  }
}

class CategorySpec extends AnyFunSuite with CategoryLaws {
  testsFor(verify_category(Category.finSetCategory, "finSetCategory"))
  testsFor(verify_category(Category.partialOrderCategory, "partialOrderCategory"))
  testsFor(verify_category(Category.diagonalCategory(Category.finSetCategory), "diagonal(finSetCategory)"))
  testsFor(verify_category(Category.productCategory(Category.finSetCategory), "product(finSetCategory)"))
  testsFor(verify_initial(Category.finSetInit, "finSetInit"))
}

trait FunctorLaws extends GivenWhenThen { this: AnyFunSuite =>

  def verify_functor[O1,A1,O2,A2](functor: Functor[O1,A1,O2,A2], name: String) = {

    test(s"$name: functorial") {
      Given(s"source cat: ${functor.s}")
      Given(s"target cat: ${functor.t}")
      Then("functorial")
      assert(functor.functorial())
      And("preserves id")
      assert(functor.preserves_id())
      And("preserves comp")
      assert(functor.preserves_comp()) }
  }
}

class FunctorSpec extends AnyFunSuite with FunctorLaws {
  testsFor(verify_functor(Functor.id(Category.finSetCategory), "id(finSetCategory)"))
  testsFor(verify_functor(Functor.chi(Category.finSetCategory), "chi(finSetCategory)"))
  testsFor(verify_functor(Functor.chi2(Category.finSetCategory), "chi2(finSetCategory)"))
}


