package category

class Category[Objects, Arrows](
  val objects: Set[Objects],
  val arrows: Set[Arrows],
  val domain: Arrows => Objects,
  val codomain: Arrows => Objects,
  val id: Objects => Arrows,
  val comp: PartialFunction[(Arrows, Arrows), Arrows]) {

  override def toString: String = {
    val objList = objects.map(x => s"       $x").mkString("\n")
    val arrList = arrows.map(x => s"       $x").mkString("\n")
    s"""Category(\n     objects={\n$objList },\n     arrows={\n$arrList }\n    )""" }

  // set of pairs of composable arrows, ie, (g, f) where comp(g,f) is defined
  val arrows2 = Category.prod2(arrows,arrows).filter(comp.isDefinedAt(_))

  // set of triples of composable arrows, ie, (h, g, f) where comp(g,f) and comp(h, g) is defined
  val arrows3 = Category.prod3(arrows,arrows,arrows).filter { case (h,g,f) => comp.isDefinedAt((g,f)) && comp.isDefinedAt((h,g)) }

  // basic category laws

  def domain_id(): Boolean = objects.forall(x => domain(id(x)) == x)

  def codomain_id(): Boolean = objects.forall(x => codomain(id(x)) == x)

  def domain_comp(): Boolean = arrows2.forall { case (g,f) => domain(comp(g,f)) == domain(f) }

  def codomain_comp(): Boolean = arrows2.forall { case (g,f) => codomain(comp(g,f)) == codomain(g) }

  def comp_associative(): Boolean = arrows3.forall { case (h,g,f) => comp(h,comp(g,f)) == comp(comp(h,g),f) }

  def comp_id(): Boolean = arrows.forall(f => comp(id(codomain(f)),f) == f && comp(f,id(domain(f))) == f)
}

// 3.4.1 The category of finite sets
case class SetArrow[A](source: Set[A], fun: A => A, target: Set[A]) {
  override def toString: String = {
    val fn = source.map( x => s"$x -> ${fun(x)}").mkString(", ")
    s"SetArrow($source, fn[$fn], $target)" }

  def canEqual(a: Any) = a.isInstanceOf[SetArrow[A]]

  def funEqual(f: A => A, g: A => A): Boolean = source.forall(x => f(x) == g(x))

  override def equals(that: Any): Boolean = that match {
    case that: SetArrow[A] => {
      that.canEqual(this) && this.source == that.source && this.target == that.target && funEqual(this.fun, that.fun) } }
}

// 4.2.1 Initial Objects
case class InitialObj[O,A](obj: O, univ: O => A)

// Category with Initial Object
class CategoryInit[Objects, Arrows](
  objects: Set[Objects],
  arrows: Set[Arrows],
  domain: Arrows => Objects,
  codomain: Arrows => Objects,
  id: Objects => Arrows,
  comp: PartialFunction[(Arrows, Arrows), Arrows],
  val initial: InitialObj[Objects,Arrows])
    extends Category[Objects, Arrows](objects, arrows, domain, codomain, id, comp) {

  override def toString: String = {
    val objList = objects.map(x => s"       $x").mkString("\n")
    val arrList = arrows.map(x => s"       $x").mkString("\n")
    s"""Category(\n     objects={\n$objList },\n     arrows={\n$arrList }\n     initialObj=$initial\n    )""" }

  def initial_object(): Boolean = objects.forall(x => domain(initial.univ(x)) == initial.obj && codomain(initial.univ(x)) == x)

  def initial_unique(i1: InitialObj[Objects,Arrows], i2: InitialObj[Objects,Arrows]): Boolean = (i1, i2) match {
    case (InitialObj(o1, univ1), InitialObj(o2, univ2)) =>
      id(o1) == comp(univ2(o1), univ1(o2)) && id(o2) == comp(univ1(o2), univ2(o1))
  }
}

object Category {

  // cartesian product of two and three sets
  def prod2[A](a: Set[A], b: Set[A]): Set[(A,A)] = a.flatMap(x => b.map(y => (x,y)))

  def prod2b[A](a: Set[A], b: Set[A]): Set[(A,A)] = for { x <- a; y <- b } yield (x,y)

  def prod3[A](a: Set[A], b: Set[A], c: Set[A]): Set[(A,A,A)] = a.flatMap(x => b.flatMap(y => c.map(z => (x,y,z))))

  def prodArrow[A]: Function2[SetArrow[A], SetArrow[A], SetArrow[(A,A)]] = {
    case (SetArrow(s1, f1, t1), SetArrow(s2, f2, t2)) => SetArrow(prod2(s1,s2), { case (x1,x2) => (f1(x1),f2(x2)) }, prod2(t1,t2)) }

  // simple diagonal category
  def diagonalCategory[A](cat: Category[Set[A],SetArrow[A]]): Category[Set[(A,A)],SetArrow[(A,A)]] = new Category(
    objects = cat.objects.map(x => prod2(x,x)),
    arrows = cat.arrows.map(f => prodArrow(f,f)),
    domain = x => x.source,
    codomain = x => x.target,
    id = x => SetArrow(x, y => y, x),
    comp = { case (SetArrow(x2, f2, y2), SetArrow(x1, f1, y1)) if y1 == x2 => SetArrow(x1, x => f2(f1(x)), y2) } )

  // cartesian product of set objects category
  def productCategory[A](cat: Category[Set[A],SetArrow[A]]): Category[Set[(A,A)],SetArrow[(A,A)]] = new Category(
    objects = cat.objects.flatMap(x => cat.objects.map(y => prod2(x,y))),
    arrows = cat.arrows.flatMap(f => cat.arrows.map(g => prodArrow(f,g))),
    domain = x => x.source,
    codomain = x => x.target,
    id = x => SetArrow(x, y => y, x),
    comp = { case (SetArrow(x2, f2, y2), SetArrow(x1, f1, y1)) if y1 == x2 => SetArrow(x1, x => f2(f1(x)), y2) } )

  val finSetCategory = {
    val a = Set(1,2)
    val b = Set(3,4,5)
    val c = Set(6,7)
    val d = Set(8,9,10,11)
    val fab = SetArrow(a, (x: Int) => x + 2, b)
    val fac = SetArrow(a, (x: Int) => x + 5, c)
    val fbc = SetArrow(b, (x: Int) => if (x < 5) x + 3 else 7, c)
    val fcd = SetArrow(c, (x: Int) => x + 3, d)

    new Category[Set[Int], SetArrow[Int]](
      objects = Set(a, b, c, d),
      arrows = Set(fab, fac, fbc, fcd),
      domain = x => x.source,
      codomain = x => x.target,
      id = x => SetArrow(x, y => y, x),
      comp = { case (SetArrow(x2, f2, y2), SetArrow(x1, f1, y1)) if y1 == x2 => SetArrow(x1, x => f2(f1(x)), y2) } ) }

  val finSetInit = {
    val null_fn: Int => Int = { case (x: Int) if false => 0 } // always fail
    val init = Set[Int]()                                     // empty set
    val a = Set(1,2)
    val b = Set(3,4,5)
    val c = Set(6,7)
    val d = Set(8,9,10,11)
    val fab = SetArrow(a, (x: Int) => x + 2, b)
    val fac = SetArrow(a, (x: Int) => x + 5, c)
    val fbc = SetArrow(b, (x: Int) => if (x < 5) x + 3 else 7, c)
    val fcd = SetArrow(c, (x: Int) => x + 3, d)

    val fia = SetArrow(init, null_fn, a)
    val fib = SetArrow(init, null_fn, b)
    val fic = SetArrow(init, null_fn, c)
    val fid = SetArrow(init, null_fn, d)

    new CategoryInit[Set[Int], SetArrow[Int]](
      objects = Set(init, a, b, c, d),
      arrows = Set(fab, fac, fbc, fcd, fia, fib, fic, fid),
      domain = x => x.source,
      codomain = x => x.target,
      id = x => SetArrow(x, y => y, x),
      comp = { case (SetArrow(x2, f2, y2), SetArrow(x1, f1, y1)) if y1 == x2 => SetArrow(x1, x => f2(f1(x)), y2) },
      initial = InitialObj(init, x => SetArrow(init, null_fn, x)) ) }

  val partialOrderCategory = new Category[Int, (Int, Int)] (
    objects = Set(1,2,3,4),
    arrows = Set((1,2), (2,3), (3,4), (1,3), (2,4)),
    domain = _._1,   // first of pair
    codomain = _._2, // second of pair
    id = x => (x,x),
    comp = { case ((x2, y2), (x1, y1)) if y1 == x2 => (x1, y2) } )
}

// 3.5 Functors
case class Functor[O1, A1, O2, A2](s: Category[O1, A1], Fo: O1 => O2, Fa: A1 => A2, t: Category[O2, A2]) {

  def functorial(): Boolean = s.arrows.forall(f => Fo(s.domain(f)) == t.domain(Fa(f)) && Fo(s.codomain(f)) == t.codomain(Fa(f)) )

  def preserves_id(): Boolean = s.objects.forall(x => Fa(s.id(x)) == t.id(Fo(x)))

  def preserves_comp(): Boolean = s.arrows2.forall { case (g,f) => Fa(s.comp(g,f)) == t.comp(Fa(g), Fa(f)) }
}

object Functor {
  import Category._

  // identity functor, defined for any category
  def id[O1, A1](cat: Category[O1, A1]): Functor[O1, A1, O1, A1] = Functor(s=cat, Fo=(x: O1) => x, Fa=(f: A1) => f, t=cat)

  def chi[A](cat: Category[Set[A], SetArrow[A]]): Functor[Set[A], SetArrow[A], Set[(A,A)], SetArrow[(A,A)]] = {
    Functor(s = cat, Fo = (x: Set[A]) => prod2(x,x), Fa = (f: SetArrow[A]) => prodArrow(f,f), t = diagonalCategory(cat)) }

  def chi2[A](cat: Category[Set[A], SetArrow[A]]): Functor[Set[A], SetArrow[A], Set[(A,A)], SetArrow[(A,A)]] = {
    Functor(s = cat, Fo = (x: Set[A]) => prod2(x,x), Fa = (f: SetArrow[A]) => prodArrow(f,f), t = productCategory(cat)) }
}

object Categories extends App { // for testing
  println("done")
}

