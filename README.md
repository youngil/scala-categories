

# Scala Categories

Scala implementation of programs and exercises from *Computational Category Theory* by Rydeheard and Burstall.


## Requires sbt

start **sbt** in top directory, then **run** or **test** at prompt.

